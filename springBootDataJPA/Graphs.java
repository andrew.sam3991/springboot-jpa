package springBootDataJPA;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Queue;
import java.util.Set;
import java.util.Stack;

public class Graphs {
	public static void main(String[] args) {

	}
}

class Word_Ladder {
	public static void main(String[] args) {
		String beginWord = "red", endWord = "tax";
		List<String> wordList = Arrays.asList("ted", "tex", "red", "tax", "tad", "den", "rex", "pee");
		int res = findLaddersLength(beginWord, endWord, wordList);
		System.out.println(res);
		System.out.println("findLadders::" + findLadders(beginWord, endWord, wordList));
	}

	static class Node {
		public String word;
		public int depth;
		public Node prev;

		public Node(String word, int depth, Node prev) {
			this.word = word;
			this.depth = depth;
			this.prev = prev;
		}
	}

	/*
	 * Input: beginWord = "hit", endWord = "cog", wordList
	 * =["hot","dot","dog","lot","log","cog"]
	 * Output:[["hit","hot","dot","dog","cog"],["hit","hot","lot","log","cog"]]
	 * Explanation: There are 2 shortest transformation sequences: "hit" -> "hot" ->
	 * "dot" -> "dog" -> "cog" "hit" -> "hot" -> "lot" -> "log" -> "cog"
	 */
	public static List<List<String>> findLadders(String beginWord, String endWord, List<String> wordList) {
		List<List<String>> result = new ArrayList<List<String>>();

		HashSet<String> unvisited = new HashSet<>();
		unvisited.addAll(wordList);

		LinkedList<Node> queue = new LinkedList<>();
		Node node = new Node(beginWord, 0, null);
		queue.offer(node);

		int minLen = Integer.MAX_VALUE;
		while (!queue.isEmpty()) {
			Node top = queue.poll();

			// top if have shorter result already
			if (result.size() > 0 && top.depth > minLen) {
				return result;
			}

			for (int i = 0; i < top.word.length(); i++) {
				String s = top.word;
				StringBuilder temp1 = new StringBuilder(s);

				for (char ch = 'a'; ch <= 'z'; ch++) {
					temp1.setCharAt(i, ch);
					String temp = temp1.toString();
					if (s.equalsIgnoreCase(temp))
						continue;

					if (temp.equals(endWord)) {
						// add to result
						List<String> aResult = new ArrayList<>();
						aResult.add(endWord);
						Node p = top;
						while (p != null) {
							aResult.add(p.word);
							p = p.prev;
						}

						Collections.reverse(aResult);
						result.add(aResult);

						// stop if get shorter result
						if (top.depth <= minLen) {
							minLen = top.depth;
						} else {
							return result;
						}
					}

					if (unvisited.contains(temp)) {
						Node n = new Node(temp, top.depth + 1, top);
						queue.add(n);
						unvisited.remove(temp);
					}
				}
			}
		}

		return result;
	}

	public static int findLaddersLength(String beginWord, String endWord, List<String> wordList) {

		boolean isPresent = false;

		Set<String> mySet = new HashSet();
		for (String s : wordList) {
			if (s.equalsIgnoreCase(endWord))
				isPresent = true;
			mySet.add(s);
		}
		if (!isPresent)
			return 0;
		Queue<String> q = new LinkedList();
		q.add(beginWord);
		int depth = 0;

		while (!q.isEmpty()) {
			depth = depth + 1;
			int size = q.size();

			while (size-- > 0) {
				String s = q.poll();

				for (int i = 0; i < s.length(); i++) {
					StringBuilder temp1 = new StringBuilder(s);
					for (char ch = 'a'; ch <= 'z'; ch++) {
						temp1.setCharAt(i, ch);
						String temp = temp1.toString();
						if (s.equalsIgnoreCase(temp))
							continue;
						if (mySet.contains(temp)) {
							q.add(temp);
							mySet.remove(temp);
						}
						if (temp.equalsIgnoreCase(endWord))
							return depth + 1;
					}
				}
			}
		}

		return 0;
	}
}

/**
 * Input: numCourses = 4, prerequisites = [[1,0],[2,0],[3,1],[3,2]]
 * Output:[0,2,1,3] Explanation: There are a total of 4 courses to take. To take
 * course 3 you should have finished both courses 1 and 2. Both courses 1 and 2
 * should be taken after you finished course 0. So one correct course order is
 * [0,1,2,3]. Another correct ordering is [0,2,1,3].
 * 
 * @author 
 *
 */
class Topological_Sort {
	public static void main(String[] args) {
		int numCourses = 4;
		int[][] prerequisites = new int[][] { { 1, 0 }, { 2, 0 }, { 3, 1 }, { 3, 2 } };
		int[] res = findOrder(numCourses, prerequisites);
		for (int i : res)
			System.out.print(i + " ");
	}

	public static int[] findOrder(int numCourses, int[][] prerequisites) {
		List<List<Integer>> adj = new ArrayList<>(numCourses);
		for (int i = 0; i < numCourses; i++)
			adj.add(i, new ArrayList<Integer>());
		for (int i = 0; i < prerequisites.length; i++)
			adj.get(prerequisites[i][1]).add(prerequisites[i][0]);

		int[] visited = new int[numCourses];
		Stack<Integer> stack = new Stack<>();// push the resultant element in the stack
		for (int i = 0; i < numCourses; i++)
			if (!DFS(i, adj, visited, stack))
				return new int[0];

		// Answer preparations
		int[] result = new int[numCourses];
		int i = 0;
		while (!stack.isEmpty()) {
			result[i++] = stack.pop();
		}
		return result;

	}

	static boolean DFS(int i, List<List<Integer>> adj, int[] visited, Stack<Integer> stack) {
		if (visited[i] == 2)
			return true;
		if (visited[i] == 1)
			return false;
		visited[i] = 1;

		for (int val : adj.get(i))
			if (!DFS(val, adj, visited, stack))
				return false;

		visited[i] = 2;
		stack.add(i);

		return true;
	}
}

/**
 * You are given an m x n grid where each cell can have one of three values:
 * 
 * 0 representing an empty cell, 1 representing a fresh orange, or 2
 * representing a rotten orange. Every minute, any fresh orange that is
 * 4-directionally adjacent to a rotten orange becomes rotten.
 * 
 * Return the minimum number of minutes that must elapse until no cell has a
 * fresh orange. If this is impossible, return -1.
 * 
 * @author 
 *
 */
class Rotting_Oranges {
	 public static void main(String args[])
	    {
	        int arr[][]={ {2,1,1} , {1,1,0} , {0,1,1} };
	        int rotting = orangesRotting(arr);
	        System.out.println("Minimum Number of Minutes Required "+rotting);
	    }

		public static int orangesRotting(int[][] grid) {
			Queue<int[]> q = new LinkedList<int[]>();
			int row = grid.length, col = grid[0].length, freshCount = 0;
			for (int i = 0; i < row; i++) {
				for (int j = 0; j < col; j++) {
					if (grid[i][j] == 2)
						q.offer(new int[] { i, j });
					if (grid[i][j] != 0)
						freshCount++;
				}
			}

			int[] dx = { 0, 0, 1, -1 };
			int[] dy = { 1, -1, 0, 0 };
			int cnt = 0, counter = 0;
			while (!q.isEmpty()) {
				int size = q.size();
				cnt += size;
				for (int i = 0; i < size; i++) {
					int[] point = q.poll();
					for (int j = 0; j < 4; j++) {
						int x = point[0] + dx[j];
						int y = point[1] + dy[j];
						if (x < 0 || y < 0 || x >= row || y >= col || grid[x][y] == 2 || grid[x][y] == 0)
							continue;
						grid[x][y] = 2;
						q.offer(new int[] { x, y });
					}
				}
				if (q.size() != 0)
					counter++;
			}
			return cnt == freshCount ? counter : -1;
		}
}
//Java program to find the size
//of the largest tree in the forest
class LargestTree {

	static int vertices;
	static LinkedList<Integer>[] adj_list;
	static int[] visited;
	LargestTree(int v){
		vertices=v;
		adj_list=new LinkedList[vertices+1];
		for(int i=0;i<vertices+1;i++)
			adj_list[i]=new LinkedList<>();
		visited=new int[vertices+1];
	}
	//A utility function to add
	//an edge in an undirected graph.
	private static void add_an_edge(int i, int j) {
		adj_list[i].add(j);
		adj_list[j].add(i);
	}
	


//A utility function to perform DFS of a
//graph recursively from a given vertex u
//and returns the size of the tree formed by u
	static int DFSUtil(int u) {
		visited[u]=1;
		int sz = 1;

//Iterating through all the nodes
		for (int i = 0; i < adj_list[u].size(); i++)
			if (visited[adj_list[u].get(i)] == 0)

				// Perform DFS if the node is
				// not yet visited
				sz += DFSUtil(adj_list[u].get(i));
		return sz;
	}

//Function to return the size of the
//largest tree in the forest given as
//the adjacency list
	static int largestTree(int V) {
		int answer = 0;

//Iterating through all the vertices
		for (int u = 0; u < V; u++) {
			if (visited[u] == 0) {
				// Find the answer
				answer = Math.max(answer, DFSUtil(u));
			}
		}
		return answer;
	}

//Driver code
	public static void main(String[] args) {
		int V = 5;
		LargestTree graph=new LargestTree(V);
		
		add_an_edge( 0, 1);
		add_an_edge(0, 2);
		add_an_edge(3, 4);
		System.out.print(largestTree(V));
	}
}

class LargestTreeInForest {
    int find(int a, int[] parent){
        if(parent[a]==-1){
            return a;
        }
        parent[a] = find(parent[a],parent);
        return parent[a];
    }
    public int countComponents(int n, int[][] edges) {
        int count=n;
        int parent[] = new int[n+1];
        Arrays.fill(parent,-1);
        for(int i=0;i<edges.length;i++){
            int x = find(edges[i][0],parent);
            int y = find(edges[i][1],parent);
            //System.out.println(x+"  "+y);
            if(x!=y){
                parent[x] = y;
                count--;
            }
        }
        
        for(int i=1;i<=n;i++){
            find(i,parent);
        }
        
        HashMap<Integer,Integer> h = new HashMap<>();
        int NoOfChildrean=0;
        int root=0;
        for(int i=1;i<=n;i++){
            int max = h.getOrDefault(parent[i],0);
            h.put(parent[i],max+1);
            if(NoOfChildrean<(max+1)){      
                NoOfChildrean = max+1;
                root = parent[i];
            }   
        }
        System.out.println(root);
        return root;
    }
}