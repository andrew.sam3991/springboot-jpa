package springBootDataJPA;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Stack;




public class Binary_Tree {
	public static void main(String[] args) {

	}
}
//Dummy data getters for trees
class TreeData{
	TreeNode root1;
	TreeNode root2;

	TreeData() {
		TreeNode r1 = new TreeNode(1);
		r1.left = new TreeNode(2);
		r1.right = new TreeNode(3);
		r1.left.left = new TreeNode(4);
		r1.left.right = new TreeNode(5);
		r1.right.left = new TreeNode(6);
		r1.right.right = new TreeNode(7);

		TreeNode r2 = new TreeNode(20);
		r2.left = new TreeNode(8);
		r2.right = new TreeNode(22);
		r2.left.left = new TreeNode(5);
		r2.left.right = new TreeNode(3);
		r2.right.left = new TreeNode(4);
		r2.right.right = new TreeNode(25);
		r2.left.right.left = new TreeNode(10);
		r2.left.right.right = new TreeNode(14);
		this.setRoot1(r1);
		this.setRoot2(r2);
	}
        
	public TreeNode getRoot1() {
		return root1;
	}
	public void setRoot1(TreeNode root1) {
		this.root1 = root1;
	}
	public TreeNode getRoot2() {
		return root2;
	}
	public void setRoot2(TreeNode root2) {
		this.root2 = root2;
	}
}

class TreeNode {
	
	TreeNode left;
	TreeNode right;
	int val;
	public TreeNode(int val) {
		this.val = val;
	}
}
 class Pair {
    int rootVal, level;

    Pair() {}
    Pair(int i, int j)
    {
        rootVal = i;
        level = j;
    }
}
/**
 * 1.Right view
 * 2.Left view
 * 3.Bottom view
 * 4.Bottom left value
 * @author 
 *
 */
class Right_Left_View_BT{
	static Map<Integer,Pair> map=new HashMap<>();

	public static void main(String[] args) {
		TreeData td = new TreeData();
		TreeNode root = td.getRoot1();
		TreeNode root2 = td.getRoot2();
		List res1 = new ArrayList<>();
		List res2 = new ArrayList<>();
		TreeNode t=null;
		int[] res=new int[1];
		res[0]=Integer.MIN_VALUE;
		maxDiff(root2,res);
		print(res[0],"MaxDiff"); 
		System.out.println("printZigZagTraversal");
		printZigZagTraversal(root);
		
		print(treePathsSum(root,0),"TreePathsSum"); 
		
		print(findDistance(root2,10,4),"FindDistance Btw to Nodes"); 
		
		int[] count=new int[1];
		count[0]=1;
		find_No_of_Leaf_Nodes(root,count);
		print(count[0],"Leaf_Node_Count"); 
		
		print(findMinDepth(root),"Find_Min_Depth");
		
		rightView(root, 0, res1);
		print(res1,"Rightview");
		
		leftView(root, 0, res2);
		print(res2,"Leftview");
		
		bottomView(root, 0,0);
		print(map,"BottomView");
		
		topView(root,0,0);
		print(map,"TopView");
		
		
		print(mergeTrees(root,root2),"mergeTrees");
		int[] lastLevel=new int[1];
		lastLevel[0]=-1;
		int[] res3 = new int[1];
		findBottomLeftValue(root, 0, res3,lastLevel);
		print(res3[0],"FindBottomLeftValue");
		
		print(height(root),"Height");
		print(diameterOfBinaryTree(root),"Diameter");
		print(isBalancedTree(root),"IsBalancedTree");
		print(lca(root,4,6).val,"LCA");
		print(isSameTree(root,root),"IsSameTree");
		
		int[] max=new int[1];
		print(maxSum(root,max),"Maxsum");
		
		TreeNode[] prev=new TreeNode[1];
        prev[0]=null;
        flatten(root,prev);//Root got changed here
     	print(root,"Flattern");
		
		t=removeHalfNodes(root);//Root got changed again
		print(t,"RemoveHalfNodes");
			 
	}



	/*Print method*/
	public static void print(Object obj,String msg) {
		System.out.print(msg+"::");
		if(obj instanceof List)
			System.out.print(obj);
		else if(obj instanceof Map) {
			for(Entry<Integer, Pair> m:map.entrySet())
				 System.out.print(m.getValue().rootVal+" ");
		}
		else if(obj instanceof TreeNode) { //For printing trees
			class Local{
				void printInorder(TreeNode node)
			    {
			        if (node != null)
			        {
			            printInorder(node.left);
			            System.out.print(node.val + " ");
			            printInorder(node.right);
			        }
			    }
			}new Local().printInorder((TreeNode)obj);
		}
		else
			System.out.print(obj);
		System.out.println();
		
	}
	/**find No of leaf nodes*/
	private static void find_No_of_Leaf_Nodes(TreeNode root,int[] count) {
		if(root==null)
			return;
		find_No_of_Leaf_Nodes(root.left,count);
		find_No_of_Leaf_Nodes(root.right,count);
		if(root.left==null && root.right==null)
			count[0]= count[0]+1;
		return;
	}


	private static void rightView(TreeNode root,int level,List res) {
		if(root==null)
			return;
		if(level==res.size())
			res.add(root.val);
		rightView(root.right,level+1,res);
		rightView(root.left,level+1,res);
		
	}
	private static void leftView(TreeNode root,int level,List res) {
		if(root==null)
			return;
		if(level==res.size())
			res.add(root.val);
		leftView(root.left,level+1,res);
		leftView(root.right,level+1,res);
		
	}
	private static void bottomView(TreeNode root,int dist,int level) {
		if(root==null)
			return;
		if(!map.containsKey(dist) || map.get(dist).level<level)
			map.put(dist,new Pair(root.val, level));
		bottomView(root.left,dist-1,level+1);
		bottomView(root.right,dist+1,level+1);
		
	}
	private static void topView(TreeNode root,int dist,int level) {
		if(root==null)
			return;
		if(!map.containsKey(dist) || map.get(dist).level>level)
			map.put(dist,new Pair(root.val, level));
		topView(root.left,dist-1,level+1);
		topView(root.right,dist+1,level+1);
		
	}
	private static int treePathsSum(TreeNode root, int total) {
		if (root == null)
			return 0;

		total = total  + root.val;
		if (root.left == null && root.right == null)
			return total;
		int left = treePathsSum(root.left, total);
		int right = treePathsSum(root.right, total);
		
		return left + right;

	}
	
	private static TreeNode removeHalfNodes(TreeNode root) {
		if(root ==null)
			return null;
		root.left=removeHalfNodes(root.left);
		root.right=removeHalfNodes(root.right);
		if(root.left==null && root.right==null)
			return root;
		
		if(root.left==null) {
			TreeNode new_node=new TreeNode(root.right.val);
			return new_node;
		}
		if(root.right==null) {
			TreeNode new_node=new TreeNode(root.left.val);
			return new_node;
		}
		return root;
	}


	private static void allLeafNodes(TreeNode root,List res) {
		if(root==null)
			return;
		if (root.left==null && root.right==null) {
			res.add(root.val);
			return;
		}
		allLeafNodes(root.left,res);
		allLeafNodes(root.right,res);
		
	}
	
	static int lastLevel=-1;
	private static void findBottomLeftValue(TreeNode root,int level,int[] res,int[] lastLevel) {
		if(root==null)
			return;
		if(lastLevel[0]<level) {
			lastLevel[0]=level;
			res[0]=root.val;
		}
		findBottomLeftValue(root.left,level+1,res,lastLevel);
		findBottomLeftValue(root.right,level+1,res,lastLevel);
		
	}

	/*We can call this as maxDepth of Binary tree */
	private static int height(TreeNode root) {
		return root != null ? Math.max(height(root.left), height(root.right)) + 1 : 0;
	}

	
	/* The diameter of a binary tree is the length of the longest path between any two nodes in a tree.*/	 
	public static int diameterOfBinaryTree(TreeNode root) {
		return height(root.left) + height(root.right) + 2;
	}
	
	public static Boolean isBalancedTree(TreeNode root) {
		if (root == null)
			return true;
		int hl = height(root.left);
		int hr = height(root.right);
		if (Math.abs(hl - hr) > 1) return false;
		Boolean left = isBalancedTree(root.left);
		Boolean right = isBalancedTree(root.right);
		if (left && right)
			return true;
		return false;
	}
	
	public static TreeNode lca(TreeNode root, int left, int right) {
		if (root == null) return null;
		if (root.val == left || root.val == right)
			return root;
		TreeNode lf = lca(root.left, left, right);
		TreeNode rf = lca(root.right, left, right);
		if (lf != null && rf != null)
			return root;
		else
			return lf != null ? lf : rf;
	}
	
    /* The path sum of a path is the sum of the node's values in the path. 
	  Given the root of a binary tree, return the maximum path sum of any path */
	private static int maxSum(TreeNode root, int[] maxi) {

		if (root == null)return 0;

		int leftSum = Math.max(0, maxSum(root.left, maxi));
		int rightSum = Math.max(0, maxSum(root.right, maxi));

		maxi[0] = Math.max(maxi[0], leftSum + rightSum + root.val);
		return Math.max(leftSum, rightSum) + root.val;

	}
/*Identical tree start*/	
	  public static boolean isSameTree(TreeNode r1, TreeNode r2) {
      	if (r1==null && r2==null) return true;
		if (r1 == null || r2 == null) return false;	    
		if(checkIdentical(r1,r2))
			return true;

		return isSameTree(r1.left, r2)|| isSameTree(r1.right, r2);
  }
    static Boolean checkIdentical(TreeNode p,TreeNode q) {
		if(p==null && q==null)
			return true;
		
		if(p!=null && q!=null)
			return p.val == q.val && checkIdentical(p.left, q.left)
					&& checkIdentical(p.right, q.right);
		return false;
	}
 /*Identical tree end*/	

	/* Symmetrical tree - Mirror tree*/
	public boolean isSymmetric(TreeNode root) {
		return checkMirror(root, root);
	}
	static Boolean checkMirror(TreeNode p,TreeNode q) {//same as checkIdentical(), only changes is on Tree1-R vs Tree2-L AND Tree1-L vs Tree2-R 
		if(p==null && q==null)
			return true;
		
		if(p!=null && q!=null)
			return p.val == q.val && checkMirror(p.left, q.right)
					&& checkMirror(p.right, q.left);
		return false;
	}
	/* Symmetrical tree  end*/
	public static void flatten(TreeNode root, TreeNode[] prev) {
		if (root == null)
			return;
		flatten(root.right, prev);
		flatten(root.left, prev);
		root.right = prev[0];
		root.left = null;
		prev[0] = root;
	}
	
	/**
	 * Given a binary tree, we need to find maximum value we can get by subtracting
	 * value of node B from value of node A, where A and B are two nodes of the
	 * binary tree and A is an ancestor of B. Expected time complexity is O(n).
	 */
	private static int maxDiff(TreeNode tr,int[] res) {
		if(tr==null)
			return Integer.MAX_VALUE;
		if(tr.left==null && tr.right==null)
		   return tr.val;
		
		int leftVal=maxDiff(tr.left,res);
		int rightVal=maxDiff(tr.right,res);
		
		int min=Math.min(leftVal, rightVal);
		res[0]=Math.max(tr.val-min, res[0]);
		
		return Math.min(tr.val, min);
	}
	

	/**
	 * Find distance between two nodes of a Binary Tree
	 * Dist(n1, n2) = Dist(root, n1) + Dist(root, n2) - 2*Dist(root, lca) 
	 * 'lca' is lowest common ancestor of n1 and n2
	 * @author 
	 *
	 */
	private static int findDistance(TreeNode root, int i, int j) {

		TreeNode lca = lca(root, i, j);
		int d1=dist(root,i,0);
		int d2=dist(root,j,0);
		int lcaDist=dist(root,lca.val,0);
		int actualDist = d1 + d2 - (2 *lcaDist);
		return actualDist;
	}
	private static int dist(TreeNode root, int key, int level) {
		if (root == null)
			return -1;
		if (root.val == key)
			return level;

		int left = dist(root.left, key, level + 1);
		if (left == -1)
			return dist(root.right, key, level + 1);
		return left;
	} 
	
	/**
	 * Given a binary tree, find its minimum depth.
	 * 
	 * The minimum depth is the number of nodes along the shortest path from the
	 * root node down to the nearest leaf node.
	 * 
	 * Note: A leaf is a node with no children.
	 */
	private static int findMinDepth(TreeNode rootNode) {
		  if(rootNode==null) return 0;
	        
			if (rootNode.left == null && rootNode.right == null)
				return 1;

			int left = rootNode.left != null ? findMinDepth(rootNode.left) : Integer.MAX_VALUE;
			int right = rootNode.right != null ? findMinDepth(rootNode.right) : Integer.MAX_VALUE;

			return Math.min(left, right) + 1;
	}

  public static TreeNode mergeTrees(TreeNode t1, TreeNode t2) {
		
			if (t1 == null)
				return t2;
			if (t2 == null)
				return t1;
			TreeNode newNode = new TreeNode(t1.val + t2.val);
			newNode.left= mergeTrees(t1.left, t2.left);
			newNode.right= mergeTrees(t1.right, t2.right);
			return newNode;  
	    }


private static void printZigZagTraversal(TreeNode root) {
	Stack<TreeNode> s1=new Stack();
	Stack<TreeNode> s2=new Stack();
	if(root!=null) {
		s1.push(root);
		while(!s1.isEmpty()) {
			TreeNode n=s1.pop();
			System.out.println(n.val);
			if(n.left!=null) s2.push(n.left);
			if(n.right!=null) s2.push(n.right);
			while(!s2.isEmpty()) {
				TreeNode n2=s2.pop();
				System.out.println(n2.val);
				if(n2.right!=null) s1.push(n2.right);
				if(n2.left!=null) s1.push(n2.left);
			}
		}
	}
	
}
}


/**
 * Delete a Node in Binary Search Tree
 * @author 
 *
 */

class Delete_Node_BST {
    public TreeNode deleteNode(TreeNode root, int key) {
        if (root == null) {
            return null;
        }
        if (root.val == key) {
            return helper(root);
        }
        TreeNode dummy = root;
        while (root != null) {
            if (root.val > key) {
                if (root.left != null && root.left.val == key) {
                    root.left = helper(root.left);
                    break;
                } else {
                    root = root.left;
                }
            } else {
                if (root.right != null && root.right.val == key) {
                    root.right = helper(root.right);
                    break;
                } else {
                    root = root.right;
                }
            }
        }
        return dummy;
    }
    public TreeNode helper(TreeNode root) {
            if (root.left == null) {
                return root.right;
            } else if (root.right == null){
                return root.left;
            } else {
                TreeNode rightChild = root.right;
                TreeNode lastRight = findLastRight(root.left);
                lastRight.right = rightChild;
                return root.left;
            }
    }
    public TreeNode findLastRight(TreeNode root) {
        if (root.right == null) {
            return root;
        }
        return findLastRight(root.right);
    }
}



