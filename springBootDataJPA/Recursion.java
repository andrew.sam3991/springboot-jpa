package springBootDataJPA;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class Recursion {
	public static void main(String[] args) {

	}
}
/**
 * Given an array nums of distinct integers, return all the possible permutations. You can return the answer in any order.
Example 1:

Input: nums = [1,2,3]
Output: [[1,2,3],[1,3,2],[2,1,3],[2,3,1],[3,1,2],[3,2,1]]
 * @author 
 *
 */
class Permutation {
	public static void main(String[] args) {
		int set[] = { 1, 2, 3 };
		System.out.println("Subset_Generate::" + permute(set));
	}

	private static List<List<Integer>> permute(int[] arr) {
		List<List<Integer>> resList = new ArrayList();
		func(0, arr, resList);
		return resList;
	}

	private static void func(int index, int[] arr, List<List<Integer>> resList) {

		if (index == arr.length) {
			List path = new ArrayList();
			for (int i : arr)
				path.add(i);

			resList.add(new ArrayList<>(path));
			return;
		}
 		for (int i = index; i < arr.length; i++) {
			swap(arr, index, i);
			func(index + 1, arr, resList);
			swap(arr, index, i);
		}
 	}

	private static void swap(int[] arr, int i, int j) {
		int temp = arr[i];
		arr[i] = arr[j];
		arr[j] = temp;
	}
}


class Permutation_String {
	public static void main(String[] args) {
		StringBuffer str=new StringBuffer("abc");
		System.out.println("Subset_Generate::" + permuteString(str));
	}
	private static List<String> permuteString(StringBuffer str) {
		List<String> resList = new ArrayList();
		strFunc(0,str, resList);
		return resList;
	}
	
	
	private static void strFunc(int index, StringBuffer str,List<String> resList) {

		if (index == str.length()) {

			resList.add(new String(str));
			return;
		}
 		for (int i = index; i < str.length(); i++) {
 			swapString(str, index, i);
 			strFunc(index + 1, str, resList);
 			swapString(str, index, i);
		}
 	}
	
	private static void swapString(StringBuffer str, int i, int j) {
		char temp = str.charAt(i);
		str.setCharAt(i, str.charAt(j));
		str.setCharAt(j, temp);
	}
}
/**
 * Subset_Generate::[[], [1], [1, 2], [1, 2, 3], [1, 3], [2], [2, 3], [3]]
 * @author 
 *
 */
class Subset_Generate{
    public static void main(String[] args) {
    	int set[] = { 1,2,3 };
   	   System.out.println("Subset_Generate::"+ subset_Generate(set));
	}

	private static List<List> subset_Generate(int[] set) {
		List<List> resList=new ArrayList();
		func2(0,set,new ArrayList<>(),resList);
		return resList;
	}

	private static void func(int index, int[] arr, ArrayList subset, List<List> resList) {
		resList.add(new ArrayList<>(subset));
		
		for(int i=index;i<arr.length;i++) {
			subset.add(arr[i]);
			func(i+1,arr,subset,resList);
			subset.remove(subset.size()-1);
		}
		
	}
   //using recursions
	private static void func2(int index, int[] arr, ArrayList subset, List<List> resList) {
		if (index == arr.length) {
			resList.add(new ArrayList<>(subset));
			return;
		}

		subset.add(arr[index]);
		func2(index + 1, arr, subset, resList);
		subset.remove(subset.size() - 1);
		func2(index + 1, arr, subset, resList);
	}
}



    

/**
 * Given an integer array nums that may contain duplicates, return all possible subsets (the power set).

The solution set must not contain duplicate subsets. Return the solution in any order.

 
Example 1:

Input: nums = [1,2,2]
Output: [[],[1],[1,2],[1,2,2],[2],[2,2]]
 * @author 
 *
 */
class Subset_Without_Duplicates{
	public static void main(String[] args) {
		 int set[] = { 1,2,2};	
		 subsetList(set);
	}
	
private static List subsetList(int[] arr) {
		
		List res=new ArrayList();
		Arrays.sort(arr);
		func1(0,arr,new ArrayList<>(),res);	
		System.out.println(res);
		return res;
		
		
	}
	
	private static List<List> func1(int index, int[] arr,List ds,List ansList) {
		ansList.add(new ArrayList<>(ds));
		for(int i=index;i<arr.length;i++) {
			if(i!=index && arr[i]==arr[i-1]) continue;
			ds.add(arr[i]);
			func1(i+1,arr,ds,ansList);
			ds.remove(ds.size()-1);
		}
		return ansList;
	}
}



/**
 * Subset Sums
 * @author 
 *
 */
class Subset_Sums{
	public static void main(String[] args) {
		 int set[] = { 1,2,3 };
		 System.out.println("subsetSums::"+ subsetSums(set));
	}

	private static ArrayList subsetSums(int[] arr) {
		
		ArrayList sumset=new ArrayList();
		func1(0,0,arr,sumset);
		return sumset;
		
		
	}
	
	private static void func1(int index, int sum, int[] arr, ArrayList<Integer> sumset) {
		sumset.add(sum);
		for (int i = index; i < arr.length; i++) {
			sum = sum + arr[i];
			func1(i + 1, sum, arr, sumset);
			sum = sum - arr[i];
		}

	}
	
	//recursive
	private static void func(int index, int sum, int[] arr, ArrayList<Integer> sumset) {
		if(arr.length==index) {
			sumset.add(sum);
			return;
		}
		func(index+1,sum+arr[index],arr,sumset);
		func(index+1,sum,arr,sumset);
		
	}
}

/**
 * Given an array of distinct integers candidates and a target integer target,
 * return a list of all unique combinations of candidates where the chosen
 * numbers sum to target. You may return the combinations in any order.
 * 
 * The same number may be chosen from candidates an unlimited number of times.
 * candidates[] = {2,3,6,7}, target = 7;
 * 
 * output::[[2, 2, 3], [7]]
 * 
 * @author 
 *
 */

class Combination_Sum_With_Repeated_index{
	public static void main(String[] args) {
	  int candidates[] = {2,3,6,7}, target = 7;
	  System.out.println("combinationSum::"+ combinationSum(candidates,target));
	}
	
	 public static List<List<Integer>> combinationSum(int[] candidates, int target) {
		 List resList=new ArrayList();
		 func(0,candidates,target,new ArrayList(),resList);
		return resList;
	        
	    }
	//Method 1:
	private static void func(int index, int[] arr,int target, ArrayList ds, List resList) {
		if (target == 0) {
			resList.add(new ArrayList<>(ds));
			return;
		}
		for (int i = index; i < arr.length; i++) {
			if (target >= arr[i]) {
				ds.add(arr[i]);
				func(i, arr, target - arr[i], ds, resList);
				ds.remove(ds.size() - 1);
			}
		}		
	}
	//Method 2:
	//Using complete Recusrion 
	private static void func2(int index, int[] arr,int target, ArrayList ds, List resList) {
		 
		 
		if(index==arr.length) {
			if(target==0)
				resList.add(new ArrayList<>(ds));
			return;
		}
		
		if (target >= arr[index]) {
			ds.add(arr[index]);
			func(index, arr, target - arr[index], ds, resList);//adding same element instead of  next element
			ds.remove(ds.size() - 1);
		}
		func(index+1,arr,target,ds,resList);
		
	}
}

/**
 * Given a collection of candidate numbers (candidates) and a target number
 * (target), find all unique combinations in candidates where the candidate
 * numbers sum to target. Each number in candidates may only be used once in the
 * combination.
 * 
 * Note: The solution set must not contain duplicate combinations.
 * 
 * Input: candidates = [10,1,2,7,6,1,5], target = 8
Output: 
[
[1,1,6],
[1,2,5],
[1,7],
[2,6]
]
 */
class Combination_Sum_WithOut_Repeated_index {

	public static void main(String[] args) {
		int candidates[] = { 10, 1, 2, 7, 6, 1, 5 }, target = 8;
		System.out.println("combinationSum::" + combinationSum2(candidates, target));
	}

	private static List<List> combinationSum2(int[] candidates, int target) {

		List<List> resList = new ArrayList();
		Arrays.sort(candidates);
		func(0,candidates,target,new ArrayList<>(),resList);
		return resList;
	}

	private static void func(int index, int[] arr, int target, ArrayList ds, List<List> resList) {
		 
		if (target == 0) {
			resList.add(new ArrayList<>(ds));
			return;
		}
		for (int i = index; i < arr.length; i++) {	
			if (i != index && arr[i] == arr[i - 1]) continue;	
			if (arr[i] < target) {
				ds.add(arr[i]);
				func(i + 1, arr, target - arr[i], ds, resList); // adding next element instead of same dupl element
				ds.remove(ds.size() - 1);
			}

		}			
  }
}

/**
 * Given a string s, partition s such that every substring of the partition is a
 * palindrome. Return all possible palindrome partitioning of s.
 * 
 * A palindrome string is a string that reads the same backward as forward.
 *  
 * Example 1:
 * 
 * Input: s = "aab" Output: [["a","a","b"],["aa","b"]]
 * 
 * @author 
 *
 */
class Palindrome_Partitioning {
	public static void main(String[] args) {
		String s = "aabbb";
		partition(s);
	}

	public static List<List<String>> partition(String s) {

		List<List<String>> resList = new ArrayList();
		func(0, s, new ArrayList<>(), resList);
		System.out.println(resList);
		return resList;
	}

	private static void func(int index, String s, ArrayList path, List<List<String>> resList) {

		if (index == s.length()) {
			resList.add(new ArrayList<>(path));
			return;
		}
		for (int i = index; i < s.length(); i++) {
			if (isPolindrome(s, index, i)) {
				path.add(s.substring(index, i + 1));
				func(i + 1, s, path, resList);
				path.remove(path.size() - 1);
			}
		}

	}

	static Boolean isPolindrome(String s, int st, int end) {
		while (st <= end) {
			if (s.charAt(st++) != s.charAt(end--))
				return false;
		}
		return true;
	}
}

class LongestPalinSubstring {

	public static void main(String[] args) {
		String str = "nmiabcddcbaqwe";
		System.out.println(longestPalindrome(str));
	}
	 public static String longestPalindrome(String s) {
	        String max = "";
	        for (int i = 0; i < s.length(); i++) {

	            String s1 = extend(s, i, i);
	            String s2 = extend(s, i, i + 1);
	            if (s1.length() > max.length()) max = s1;
	            if (s2.length() > max.length()) max = s2;
	         
	        }
	        return max;
	    }
	    
	    private static String extend(String s, int i, int j) {
	        for (; i>=0 && j < s.length(); i--, j++) {
	            if (s.charAt(i) != s.charAt(j)) break;
	        }
	        return s.substring(i + 1, j);
	    }
}


class Interleaving_leaving{
	public static void main(String[] args) {
		String s1 = "aabcc", s2 = "dbbca", s3 = "aadbbbaccc";
		System.out.println(isInterleave(s1, 0, s2, 0, s3, 0));
	}
	private static boolean isInterleave(String s1, int i1, String s2, int i2, String s3, int i3)
	{	
		if(i1==s1.length())
			return s3.substring(i3).equals(s2.substring(i2));
		if(i2==s2.length())
			return s3.substring(i3).equals(s1.substring(i1));
	   if(s3.charAt(i3)==s1.charAt(i1) && isInterleave(s1,i1+1,s2,i2,s3,i3+1) ||
			   s3.charAt(i3)==s2.charAt(i2) && isInterleave(s1, i1, s2, i2+1, s3, i3+1))
		   return true;
	   
	   return false;
	}
}

/**
 * Given two strings text1 and text2, return the length of their longest common
 * subsequence. If there is no common subsequence, return 0.
 * 
 * A subsequence of a string is a new string generated from the original string
 * with some characters (can be none) deleted without changing the relative
 * order of the remaining characters.
 * 
 * For example, "ace" is a subsequence of "abcde". A common subsequence of two
 * strings is a subsequence that is common to both strings.
 */

/**
 * �ABCDGH� and �AEDFHR�
 *
 */
 class Longest_common_subsequence {
	public static void main(String[] args) {
		String s1="BD";
		String s2= "ABCD";
		System.out.println(LCS(s1,s2,0,0));
		longestCommonSubsequence(s1,s2);
	}

	//Recursive method
	private static int LCS(String s1, String s2,int i,int j) {
		if (i == s1.length() || j == s2.length())
			return 0;
		else if (s1.charAt(i) == s2.charAt(j))
			return 1 + LCS(s1, s2, i+1, j+1);
		else
			return Math.max(LCS(s1, s2, i+1, j), LCS(s1, s2, i, j + 1));
	}
	
	
	//Dynamic Programming 
	private static void longestCommonSubsequence(String n1, String n2) {
		n1=" "+n1;
		n2=" "+n2;
		int T[][]=new int[n1.length()][n2.length()];
		
		for(int i=1;i<n1.length();i++) {
			char c=n1.charAt(i);
			for(int j=1;j<n2.length();j++) {
				if(c!=' ' && c==n2.charAt(j)) 
					 T[i][j]=T[i-1][j-1]+1;
				else
					 T[i][j]=Math.max(T[i-1][j], T[i][j-1]);
					
				}
			}
		System.out.println(T[n1.length()-1][n2.length()-1]);//length of the longest common sequence 
		String res="";
		int i=n1.length()-1;
		int j=n2.length()-1;
				
			while(j>=1 && i>=1) {
				int k=T[i][j];
				if(k==T[i-1][j-1]+1) {
					res=res+n1.charAt(i);
					i=i-1;
					j=j-1;
				}else {
					if(k==T[i-1][j]) {
						i=i-1;
						j=j;
					}
					else {
						j=j-1;
						i=i;
					}											
				}
			}
		System.out.println(res); //longest common subsequence
		}
 }
class Word_Break{
	
	public static void main(String[] args) {
		String s = "code";
		List wordDict =new ArrayList<>(Arrays.asList("c","od","e"));
		System.out.println("wordBreak::"+wordBreak(s,wordDict));
	}
	 public static boolean wordBreak(String s, List<String> set) {
		 Set prevDictWords=new HashSet<>();
		 return func(s,set,prevDictWords);
	 }
	
		public static boolean func(String s, List<String> dict, Set prevDictWords) {
			if (dict.contains(s))
				return true;
			for (int i = 0; i < s.length(); i++) {
				String firstPart = s.substring(0, i);
				String secondPart = s.substring(i);
				if (dict.contains(firstPart) && prevDictWords.contains(secondPart))
					return true; // memorizations
				if (dict.contains(firstPart) && func(secondPart, dict, prevDictWords)) {
					prevDictWords.add(secondPart); // adding val for memorization
					return true;
				}
			}
			return false;

		}
}
